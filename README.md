VKStore was an e-commerce store idea formed towards the end of 2018 with my business partner Casey Huang. And we lunched the business at the start of 2019. We built the brand and sourced the suppliers from local and also from china. The business started with a huge success and we believe that if we had more investment and opened a brick and mortar shop in Wellington, we it would have grown even more. But towards the end of 2019 we dissolved the e-store because of personal reason despite its success. I moved to USA to undertake master’s in Software Engineering while my business partner got a job as a Software Engineer in Sydney. Having no one in New Zealand to operate logistics, provide customer support and ship the order. It was decided that it was best to run a closing down sell and close VKStore. 

If you are interested in starting your own e-commerce store and are lost or confused on how to get started please reach out to me dipen70@gmail.com. This was also my first e-store and I went brought all the hardships will happily provide insight and tips and tricks based on what I have learnt while building and operating VKStore. 

If you’re interested in buying our domain http://vkstore.co.nz/ please contact me on the email above.

 
